#pragma once

#define COMBO_COUNT 1

// M101 16+12
#define MSTAR_LED_PINS { B9, B8, B7 }
#define MSTAR_STATUS_LED A14 // SWCLK
#define LED_NUM_LOCK_PIN B9
#define LED_CAPS_LOCK_PIN B8
#define LED_SCROLL_LOCK_PIN B7
#define LED_PIN_ON_STATE 0

// A14 has the function of SWDIO and A13 has the function of SWCLK
// SWDIO requires a 10k-100k pullup and SWCLK a 10k-100k pulldown
// on SOME (but not all) BluePills...

// 3ma Limit. Don't actually use this.
//#define MODELJ_STATUS_LED A14  // SWCLK


/* key matrix size */
#undef MATRIX_ROWS
#undef MATRIX_COLS
#define MATRIX_ROWS 12
#define MATRIX_COLS 16

// M122 '7000
//#define MATRIX_COLS 20
#define NO_DEBUG
/*
 * Keyboard Matrix Assignments
 *
 * Change this to how you wired your keyboard
 * COLS: AVR pins used for columns, left to right
 * ROWS: AVR pins used for rows, top to bottom
 * DIODE_DIRECTION: COL2ROW = COL = Anode (+), ROW = Cathode (-, marked on diode)
 *                  ROW2COL = ROW = Anode (+), COL = Cathode (-, marked on diode)
 *
 */

//#define MATRIX_ROW_PINS { B6, B5, B4, B3, A15, A0, A2, A1 }
//#define MATRIX_ROW_PINS { A1, A2, A0, B3, A15, B4, B5, B6 }
//#define MATRIX_ROW_PINS { B6, B5, B4, A15, B3, A0, A2, A1 }

// BODGED v0.01  OK (offset 4 pins on 2nd row)
//#define MATRIX_ROW_PINS { B8, B7, C14, C15, A1, A2, A0, B3 }

// v0.02
//#define MATRIX_ROW_PINS { B6, B5, B4, A15, B3, A0, A2, A1, C15, C14, C13, A13 }
#undef MATRIX_ROW_PINS
#define MATRIX_ROW_PINS { B6, B5, B4, A15, B3, A0, A2, A1, C15, C14, C13, A13 }

// v0.01 OK
#undef MATRIX_COL_PINS
#define MATRIX_COL_PINS { B12, B13, B14, B15, A8, A9, A10, B11, B10, B1, B0, A7, A6, A5, A4, A3 }

// v0.02
//#define MATRIX_COL_PINS { C15, C14, C13, A14, B12, B13, B14, B15, A8, A9, A10, B11, B10, B1, B0, A7, A6, A5, A4, A3 }
